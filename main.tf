# bootstrap consul with converge
variable "do_token" {}

variable "pubkey" {
  default = "~/.ssh/id_rsa.pub"
}

variable "key" {
  default = "~/.ssh/id_rsa"
}

variable "scale" {
  default = 3
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_ssh_key" "key" {
  name       = "Converge Consul"
  public_key = "${file(var.pubkey)}"
}

resource "digitalocean_droplet" "consul" {
  count = "${var.scale}"

  name               = "consul-${count.index}"
  image              = "centos-7-0-x64"
  region             = "nyc3"
  size               = "512mb"
  private_networking = true
  ssh_keys           = ["${digitalocean_ssh_key.key.id}"]
}

resource "template_file" "ips" {
  template = "${join("|", digitalocean_droplet.consul.*.ipv4_address_private)}"
}

resource "null_resource" "bootstrap_consul" {
  count = "${var.scale}"

  depends_on = ["digitalocean_droplet.consul"]

  triggers = {
    converge         = "${sha1(file("converge"))}"
    consul_bootstrap = "${sha1(file("consul-bootstrap.hcl"))}"
    ips              = "${template_file.ips.rendered}"
  }

  connection {
    type        = "ssh"
    user        = "root"
    host        = "${element(digitalocean_droplet.consul.*.ipv4_address, count.index)}"
    private_key = "${file(var.key)}"
  }

  // copy converge
  provisioner "file" {
    source      = "converge"
    destination = "/usr/bin/converge"
  }

  // copy configuration
  provisioner "file" {
    source      = "consul-bootstrap.hcl"
    destination = "~/consul-bootstrap.hcl"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /usr/bin/converge",
      "converge apply -p scale=${var.scale} -p ip=${element(digitalocean_droplet.consul.*.ipv4_address_private, count.index)} -p peers='${template_file.ips.rendered}' ~/consul-bootstrap.hcl",
    ]
  }

  // what I'd like to see in the future
  # provisioner "converge" {
  #   modules = ["~/consul-bootstrap.hcl"]
  #   params {
  #     scale = "${var.scale}"
  #     ip = "${element(digitalocean_droplet.consul.*.ipv4_address_private, count.index)}"
  #     ips = "${template_file.ips.rendered}"
  #   }
  # }
}
