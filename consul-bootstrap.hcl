# bootstrap consul
param "scale" {}

param "ip" {}

param "peers" {}

template "consul-repo" {
  destination = "/etc/yum.repos.d/asteris.repo"

  content = <<EOF
[asteris]
name=asteris
baseurl=https://dl.bintray.com/asteris/mantl-rpm
gpgcheck=0
repo_gpgcheck=0
enabled=1
EOF
}

task "jq" {
  check = "test -f /usr/bin/jq"
  apply = "curl -L https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 > /usr/bin/jq && chmod +x /usr/bin/jq"
}

task "consul" {
  check = "yum list install consul"
  apply = "yum install -y consul"

  depends = ["template.consul-repo"]
}

task "consul-bootstrap" {
  check = "jq -e '.bootstrap_expect' /etc/consul/consul.json && jq -e '.retry_join' /etc/consul/consul.json && jq -e '.bind_addr' /etc/consul/consul.json"

  apply = <<EOF
jq --argjson scale '{{param "scale"}}' \
   --arg ip '{{param `ip`}}' \
   --arg peers '{{param `peers`}}' \
   '.bootstrap_expect = $scale | .bind_addr = $ip | .retry_join = ($peers | split("|"))' < /etc/consul/consul.json > /tmp/consul.json && \
mv /tmp/consul.json /etc/consul/consul.json
EOF

  depends = ["task.consul", "task.jq"]
}

task "enable-consul" {
  check   = "systemctl is-enabled consul"
  apply   = "systemctl enable consul"
  depends = ["task.consul"]
}

task "start-consul" {
  check   = "systemctl status consul | grep Active | tee /dev/stderr | grep running"
  apply   = "systemctl start consul"
  depends = ["task.consul-bootstrap"]
}
