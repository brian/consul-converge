# Converge Consul

Steps to get started:

1. Fill in `terraform.tfvars` with your auth credentials for DigitalOcean
2. Put the converge binary you want at `converge` (should be built for linux, 64
   bit.) You can make this with `GOOS=linux GOARCH=amd64 make converge` from the
   converge project.
3. Run `terraform apply`
4. SSH to one of the nodes and verify Consul came up by running `consul members`
